package hust.soict.globalict.lab02.Lab02;

import java.util.Scanner;  

public class CreateTriangle {
    public static void main(String args[])   
{    
//i for rows and j for columns      
//row denotes the number of rows you want to print  
int i, j,e, row ; 
System.out.print("Enter the number of rows you want to print: ");  
Scanner sc = new Scanner(System.in);  
row = sc.nextInt();        
//Outer loop work for rows  
for (i=0; i<row; i++)   
{  
//inner loop work for space      
for (e=row-i; e>1; e--)   
{  
//prints space between two stars  
System.out.print(" ");   
}   
//inner loop for columns  
for (j=0; j<=i; j++ )   
{   
//prints star      
System.out.print("* ");   
}   
System.out.println();   
}   
}   
}
