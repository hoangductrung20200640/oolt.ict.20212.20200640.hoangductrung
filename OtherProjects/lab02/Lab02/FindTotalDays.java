package hust.soict.globalict.lab02.Lab02;



import java.util.Scanner;
public class FindTotalDays {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int daysinMonth=0;
        int MonthInt = 0;
        String MonthName = "Invalid";

        System.out.println("Enter the Month : ");
        String month = input.nextLine();

        System.out.println("Enter the Year: ");
        int year = input.nextInt();

        if ((month.equals("1")) || (month.equals("Jan")) || (month.equals("January")) || (month.equals("Jan.")) )
        {
           MonthInt = 1;
        }
        if ((month.equals("2")) || (month.equals("Feb")) || (month.equals("February")) || (month.equals("Feb.")) )
        {
           MonthInt = 2;
        }
        if ((month.equals("3")) || (month.equals("Mar")) || (month.equals("March")) || (month.equals("Mar.")) )
        {
           MonthInt = 3;
        }
        if ((month.equals("4")) || (month.equals("Apr")) || (month.equals("April")) || (month.equals("Apr.")) )
        {
           MonthInt = 4;
        }
        if ((month.equals("5")) || (month.equals("May"))  )
        {
           MonthInt = 5;
        }
        if ((month.equals("6")) || (month.equals("June")) || (month.equals("Jun"))  )
        {
           MonthInt = 6;
        }
        if ((month.equals("7")) || (month.equals("July")) || (month.equals("Jul")) )
        {
           MonthInt = 7;
        }
        if ((month.equals("8")) || (month.equals("Aug")) || (month.equals("August")) || (month.equals("Aug.")) )
        {
           MonthInt = 8;
        }
        if ((month.equals("9")) || (month.equals("Sep")) || (month.equals("September")) || (month.equals("Sep.")) )
        {
           MonthInt = 9;
        }
        if ((month.equals("10")) || (month.equals("Oct")) || (month.equals("October")) || (month.equals("Oct.")) )
        {
           MonthInt = 10;
        }
        if ((month.equals("11")) || (month.equals("Nov")) || (month.equals("November")) || (month.equals("Nov.")) )
        {
           MonthInt = 11;
        }
        if ((month.equals("12")) || (month.equals("Dec")) || (month.equals("December")) || (month.equals("Dec.")) )
        {
           MonthInt = 12;
        }
        switch (MonthInt) {

            case 1:
                MonthName = "January";
                daysinMonth = 31;
                break;
            case 2:
                MonthName = "February";
                if ((year % 400 == 0) || ((year % 100 != 0) && (year % 4 == 0))) {
                    daysinMonth = 29;
                    break;
                } else {
                    daysinMonth = 28;
                    break;
                }
            case 3:
                MonthName = "March";
                daysinMonth = 31;
                break;
            case 4:
                MonthName = "April";
                daysinMonth = 30;
                break;
            case 5:
                MonthName = "May";
                daysinMonth = 31;
                break;
            case 6:
                MonthName = "June";
                daysinMonth = 30;
                break;
            case 7:
                MonthName = "July";
                daysinMonth = 31;
                break;
            case 8:
                MonthName = "August";
                daysinMonth = 31;
                break;
            case 9:
                MonthName = "September";
                daysinMonth = 30;
                break;
            case 10:
                MonthName = "October";
                daysinMonth = 31;
                break;
            case 11:
                MonthName = "November";
                daysinMonth = 30;
                break;
            case 12:
                MonthName = "December";
                daysinMonth = 31;
                break;
        }
        System.out.println(MonthName+" "+year+" has "+daysinMonth+" days\n");
    }
}