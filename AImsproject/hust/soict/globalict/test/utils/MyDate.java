package hust.soict.globalict.test.utils;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Scanner;

public class MyDate{
	private static String[] months = {"January", "February", "March", "April", "May", "June", "July",         
				   						"August", "September", "October", "November", "December"};
	private int day;
	private int month;
	private int year;
	
	public MyDate() {
		this.day = LocalDate.now().getDayOfMonth();
		this.month = LocalDate.now().getMonthValue();
		this.year = LocalDate.now().getYear();
	}
	
	public MyDate(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}

	
	public MyDate(String _date) {
		this.setDateString(_date);
	}

	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		this.day = day;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	
	public void setDateString(String _date) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMMM dd'th' yyyy", Locale.US);
		LocalDate date = LocalDate.parse(_date,formatter);
		this.day = date.getDayOfMonth();
		this.month = date.getMonthValue();
		this.year = date.getYear();
	}
	
	public void accept() {
		System.out.println("Enter date: ");
		Scanner sc = new Scanner(System.in);
		String date = sc.nextLine();
		this.setDateString(date);
		sc.close();
	}
	
	public String toString() {
		return LocalDate.of(year, month, day).toString();
	}
	
	public void print() {
		System.out.println(String.format("%s %dth %s", months[this.month-1], this.day, this.year));
	}
	
	public void printCustomFormat() {
		System.out.println("Choose date format: ");
		System.out.println("1.yyyy-MM-dd\n2.d/M/yyyy\n3.dd-MMM-yyyy\n4.MMM d yyyy\n5.mm-dd-yyyy");
		Scanner sc = new Scanner(System.in);
		int choice = sc.nextInt();
		sc.nextLine();
		switch (choice) {
		case 1:
			System.out.println(String.format("%d-%d-%d",this.year, this.month, this.day));
			break;
		case 2:
			System.out.println(String.format("%d/%d/%d",this.day, this.month, this.year));
			break;
		case 3:
			System.out.println(String.format("%d-%s-%d", this.day, months[this.month-1].substring(0, 4), this.year));
			break;
		case 4:
			System.out.println(String.format("%s %d %d", months[this.month-1].substring(0, 4), this.day ,this.year));
			break;
		case 5:
			System.out.println(String.format("%d-%d-%d",this.month, this.day, this.year));
			break;
		default:
			System.out.println("ERROR: undefined option.");
		}
		
		sc.close();
	}

	
}
