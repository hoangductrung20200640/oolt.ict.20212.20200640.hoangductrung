package hust.soict.globalict.aims;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.order.Order;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.media.Book;

import java.util.ArrayList;
import java.util.Scanner;




public class Aims {
    public static void main(String[] args) {

        MemoryDaemon obj = new MemoryDaemon();
        Thread newThread = new Thread(obj);
        newThread.setDaemon(true);
        newThread.start();
        
        Book book1 = new Book("Harry Potter", "Fiction");
        book1.addAuthor("J.K Rowling");



        
		ArrayList<Media> itemList = new ArrayList<Media>();
		itemList.add(book1);
		

		Scanner sc = new Scanner(System.in);
		Order order = null;
		boolean orderCreated = false;

		while (true) {
			showMenu();
			int choice;
			choice = sc.nextInt();
			switch (choice) {
			case 0:
				sc.close();
				System.exit(0);
			case 1:
				order = new Order();
				orderCreated = true;
				System.out.println("Order created");
				break;
			case 2:
				if (!orderCreated) {
					System.out.println("Please create a new order first!");
					break;
				}
				int itemChoice;
				listItem(itemList);
				System.out.println("Choose item number: ");
				itemChoice = sc.nextInt();
				if (itemChoice <= 0 || itemChoice > itemList.size()) {
					System.out.println("Invalid index!");
					break;
				}
				order.addMedia(itemList.get(itemChoice - 1));
				break;
			case 3:
				if (!orderCreated) {
					System.out.println("Please create a new order first!");
					break;
				}
				int itemID;
				System.out.println("Enter item ID: ");
				itemID = sc.nextInt();
				order.removeMedia(itemID);
				break;
			case 4:
				if (order == null) {
					System.out.println("Please create a new order first!");
					break;
				}
				order.printOrder();
				break;
			default:
				sc.close();
				System.exit(1);
			}
			
		}
	}
	
	public static void showMenu() {
		System.out.println("Order Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4");
	}
	
	public static void listItem(ArrayList<Media> itemList) {
		for (int i = 0; i < itemList.size(); i++) {
			System.out.println(String.format("ID: %d %s", itemList.get(i).getID(), itemList.get(i)));
		}
	}



   

     



    }

    

