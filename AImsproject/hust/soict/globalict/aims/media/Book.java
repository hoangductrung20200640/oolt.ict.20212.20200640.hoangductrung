package hust.soict.globalict.aims.media;

import java.util.ArrayList;
import java.util.List;

public class Book extends Media{
  

    private List<String> authors = new ArrayList<String>();
	
	public void addAuthor(String authorName) {
		if (!authors.contains(authorName)) {
			authors.add(authorName);
			return;
		}
		System.out.println("ERROR: Author name is already in the list.");
	}
	
	public void removeAuthor(String authorName) {
		if (!authors.contains(authorName)) {
			System.out.println("ERROR: Author name not found!");
			return;
		}
		authors.remove(authorName);
		System.out.println("INFO: Author name was successfully removed from the list.");
	}


	public Book(String title) {
		super(title);
	}
	
	public Book(String title, String category) {
		super(title, category);
	}
	
	public Book(String title, String category, List<String> authors) {
		super(title, category);
		this.authors = authors;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("[Book] Title: %s, Category: %s, Author(s):", this.getTitle(), this.getCategory()));
		for (String author : this.authors) {
			sb.append(author);
		}
		return sb.toString();
	}
}
