package hust.soict.globalict.aims.media;
import java.util.ArrayList;

public class CompactDisc extends Disc {
    private String artist;
    private ArrayList <Track> tracks = new ArrayList<Track>();
    public String getArtist() {
        return artist;
    }
    public CompactDisc(String title, String artist, ArrayList<Track> tracks) {
        super(title);
        this.artist = artist;
        this.tracks = tracks;
    }
    public CompactDisc(String title, String category, String artist, ArrayList<Track> tracks) {
        super(title, category);
        this.artist = artist;
        this.tracks = tracks;
    }
    public CompactDisc(String title, String director, int length, String artist, ArrayList<Track> tracks) {
        super(title, director, length);
        this.artist = artist;
        this.tracks = tracks;
    }
    public CompactDisc(String title, String category, String director, int length, String artist,
            ArrayList<Track> tracks) {
        super(title, category, director, length);
        this.artist = artist;
        this.tracks = tracks;
    }

    public void addTrack (String title, int length) {
        for (int i = 0; i < tracks.size(); i ++ ){
            if (tracks.get(i).getTitle().equals(title)){
                System.out.println("This track already exists");
                return ;
            }
        }

        Track newDisc = new Track(title,length);
        tracks.add(newDisc);
        System.out.println("New Track has been added");
    }

    public void removeTrack (String title, int length) {
        for (int i = 0; i < tracks.size(); i ++ ){
            if (tracks.get(i).getTitle().equals(title)){
                
                tracks.remove(i);
                System.out.println("This track has been reomoved");
                return ;
            }
        }

       
        System.out.println(" No track found");
    }
    public int getLength(){
        int sumLength = 0;
        for (int i = 0; i < tracks.size(); i ++ ){
            sumLength = sumLength + tracks.get(i).getLengthk();
        }
        return sumLength;

    }

}

